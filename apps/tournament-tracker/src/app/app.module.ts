import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import localeEn from '@angular/common/locales/en';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslocoService } from '@ngneat/transloco';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { BASE_PATH } from '@tournament-tracker/api';
import { AppConfigState, FeatureResolver, FeatureState, httpInterceptors } from '@tournament-tracker/common';
import { I18nModule } from '@tournament-tracker/i18n';
import { OAuthModule } from 'angular-oauth2-oidc';
import { environment as env } from '../environments/environment';

import { AppComponent } from './app.component';
import { AuthModule } from '@tournament-tracker/auth';

registerLocaleData(localeDe);
registerLocaleData(localeEn);

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NgxsModule.forRoot([FeatureState, AppConfigState], {
            developmentMode: !env.production,
            selectorOptions: {
                suppressErrors: false,
                injectContainerState: false,
            },
        }),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsStoragePluginModule.forRoot({
            key: ['appConfig'],
        }),
        RouterModule.forRoot([
            {
                path: '',
                resolve: {
                    feature: FeatureResolver,
                },
                children: [
                    {
                        path: '',
                        loadChildren: () => import('@tournament-tracker/ui-primeng').then((m) => m.LandingModule),
                    },
                    {
                        path: 'faq',
                        loadChildren: () => import('@tournament-tracker/ui-primeng').then((m) => m.FaqModule),
                    },
                    {
                        path: 'matches',
                        loadChildren: () => import('@tournament-tracker/ui-primeng').then((m) => m.GameplansModule),
                    },
                ],
            },
            {
                path: '**',
                redirectTo: '',
            },
        ]),
        OAuthModule.forRoot({
            resourceServer: {
                allowedUrls: [`${env.apiUrl}/api/Matches`, `${env.apiUrl}/api/Teams`],
                sendAccessToken: true,
            },
        }),
        AuthModule,
        I18nModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: env.production,
            // Register the ServiceWorker as soon as the application is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000',
        }),
    ],
    providers: [
        ...httpInterceptors,
        {
            provide: LOCALE_ID,
            useFactory: (translate: TranslocoService) => {
                return translate.getActiveLang();
            },
            deps: [TranslocoService],
        },
        { provide: BASE_PATH, useValue: env.apiUrl },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
