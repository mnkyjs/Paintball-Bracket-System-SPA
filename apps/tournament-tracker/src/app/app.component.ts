import { Component, inject, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { AUTH_CONFIG } from '../auth.config';

@Component({
    selector: 'tta-root',
    template: ` <router-outlet></router-outlet> `,
})
export class AppComponent implements OnInit {
    private readonly oAuthService = inject(OAuthService);

    ngOnInit(): void {
        this.oAuthService.configure(AUTH_CONFIG);
        this.oAuthService.loadDiscoveryDocument()?.then(() => this.oAuthService.tryLoginCodeFlow());
    }
}
