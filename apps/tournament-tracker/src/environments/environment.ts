export const environment = {
    production: true,
    apiUrl: 'https://localhost:5001',
    DOMAIN: '',
    CLIENT_ID: '',
    audience: '',
};
