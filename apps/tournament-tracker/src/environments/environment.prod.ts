export const environment = {
    production: true,
    apiUrl: 'https://localhost',
    DOMAIN: '',
    CLIENT_ID: '',
    audience: '',
};
