import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from './environments/environment';

export const AUTH_CONFIG: AuthConfig = {
    issuer: environment.DOMAIN,

    clientId: environment.CLIENT_ID,

    redirectUri: window.location.origin,

    // Scopes ("rights") the Angular application wants get delegated
    scope: 'openid profile offline_access',

    // Using Authorization Code Flow
    // (PKCE is activated by default for authorization code flow)
    responseType: 'code',

    // Your Auth0 account's logout url
    // Derive it from your application's domain
    logoutUrl: `${environment.DOMAIN}v2/logout`,

    customQueryParams: {
        // API identifier configured in Auth0
        audience: environment.audience,
    },
};
