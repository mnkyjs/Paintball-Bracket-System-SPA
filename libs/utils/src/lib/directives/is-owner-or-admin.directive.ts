import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { OAuthService } from 'angular-oauth2-oidc';

@Directive({
    selector: '[ttIsOwnerOrAdmin]',
    standalone: true,
})
export class IsOwnerOrAdminDirective implements OnInit {
    helper = new JwtHelperService();

    private identifier: string | undefined | null;

    constructor(private viewContainerRef: ViewContainerRef, private templateRef: TemplateRef<any>, private oAuthService: OAuthService) {}

    @Input()
    set ttIsOwnerOrAdmin(userIdentifier: string | undefined | null) {
        this.identifier = userIdentifier;
    }

    ngOnInit(): void {
        const user = this.helper.decodeToken(this.oAuthService.getAccessToken());

        if (user) {
            if (user.sub === this.identifier || user['@roles']?.includes('admin')) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainerRef.clear();
            }
        }
    }
}
