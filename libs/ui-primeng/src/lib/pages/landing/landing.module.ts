import { NgModule } from '@angular/core';
import { LandingComponent } from './components/landing/landing.component';

import { LandingRoutingModule } from './landing-routing.module';

@NgModule({
    declarations: [LandingComponent],
    imports: [LandingRoutingModule],
})
export class LandingModule {}
