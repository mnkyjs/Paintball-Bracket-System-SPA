import { Component } from '@angular/core';

@Component({
    selector: 'tt-ui-prime-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss'],
})
export class LandingComponent {}
