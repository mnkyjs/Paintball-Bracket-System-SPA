import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameplansListViewComponent } from './gameplans-list-view.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxsModule } from '@ngxs/store';

describe('GameplansListViewComponent', () => {
    let component: GameplansListViewComponent;
    let fixture: ComponentFixture<GameplansListViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GameplansListViewComponent],
            imports: [HttpClientTestingModule, NgxsModule.forRoot()],
        }).compileComponents();

        fixture = TestBed.createComponent(GameplansListViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
