import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { MatchesService, MatchForViewDto } from '@tournament-tracker/api';
import { AuthState } from '@tournament-tracker/auth';
import { catchError, EMPTY, map, Observable } from 'rxjs';

@Component({
    selector: 'tt-ui-prime-gameplans-list-view',
    templateUrl: './gameplans-list-view.component.html',
    styleUrls: ['./gameplans-list-view.component.scss'],
})
export class GameplansListViewComponent implements OnInit {
    @Select(AuthState.isLoggedIn)
    isLoggedIn$?: Observable<boolean>;

    matches$?: Observable<MatchForViewDto[]>;

    constructor(private matchesService: MatchesService, private router: Router) {}

    ngOnInit() {
        this.matches$ = this.matchesService.getAllMatches().pipe(
            map((matchForViewDto: MatchForViewDto[]) =>
                matchForViewDto.map((matchForViewDto: MatchForViewDto) => ({
                    ...matchForViewDto,
                    date: new Date(Date.parse(<string>matchForViewDto.date)).toUTCString(),
                })),
            ),
            catchError(() => EMPTY),
        );
    }

    onMatchClick(guid: string) {
        return this.router.navigate(['/matches/details'], { queryParams: { guid } });
    }
}
