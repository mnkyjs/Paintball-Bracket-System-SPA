import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import {
    AddMatch,
    AddTeam,
    CreateScheduleDto,
    DeleteTeam,
    GetTeams,
    Metadata,
    PageEvent,
    TeamDto,
    TeamsState,
    UpdateTeam,
} from '@tournament-tracker/api';
import { ConfirmationService, ConfirmEventType, MessageService } from 'primeng/api';
import { debounceTime, distinctUntilChanged, Observable, Subject, takeUntil } from 'rxjs';

@Component({
    selector: 'tt-ui-prime-gameplans-create-view',
    templateUrl: './gameplans-create-view.component.html',
    styleUrls: ['./gameplans-create-view.component.scss'],
    animations: [
        trigger('fadeSlideInOut', [
            transition(':enter', [
                style({ opacity: 0, transform: 'translateY(10px)' }),
                animate('500ms', style({ opacity: 1, transform: 'translateY(0)' })),
            ]),
            transition(':leave', [animate('500ms', style({ opacity: 0, transform: 'translateY(10px)' }))]),
        ]),
    ],
})
export class GameplansCreateViewComponent implements OnInit, OnDestroy {
    @Select(TeamsState.metadata)
    metadata$?: Observable<Metadata>;

    @Select(TeamsState.teams)
    teams$?: Observable<TeamDto[]>;

    isModalVisible = false;

    matchForm!: FormGroup<{
        date: FormControl<Date>;
        name: FormControl<string>;
        location: FormControl<string | null>;
    }>;

    searchForm = new FormGroup({ search: new FormControl() });

    teamForm: FormGroup<{
        id: FormControl<number | null>;
        name: FormControl<string>;
        userIdentifier: FormControl<string | null>;
    }> = new FormGroup({
        id: new FormControl<number | null>(null, { nonNullable: false }),
        name: new FormControl<string>('', {
            validators: [Validators.required, Validators.min(2)],
            nonNullable: true,
        }),
        userIdentifier: new FormControl<string | null>(null, {
            nonNullable: false,
        }),
    });

    selectedTeams: TeamDto[] = [];

    metadata?: Metadata;

    private onDestroy: Subject<boolean> = new Subject<boolean>();

    constructor(
        private store: Store,
        private builder: FormBuilder,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
    ) {}

    ngOnInit() {
        this.matchForm = this.builder.group({
            name: new FormControl<string>('', { validators: Validators.required, nonNullable: true }),
            date: new FormControl<Date>(new Date(), { validators: Validators.required, nonNullable: true }),
            location: new FormControl<string | null>(null, { nonNullable: false }),
        });

        this.searchForm
            .get('search')
            ?.valueChanges.pipe(debounceTime(1000), distinctUntilChanged(), takeUntil(this.onDestroy))
            .subscribe((data: string) => {
                this.onSearch(data);
            });
    }

    onPageChange($event: PageEvent): void {
        this.store.dispatch(
            new GetTeams({
                parameters: {
                    page: $event.page,
                    pageSize: $event.rows,
                    filter: undefined,
                    sortOrder: undefined,
                    sortColumn: undefined,
                },
            }),
        );
    }

    onSearch(data: string) {
        this.store.dispatch(
            new GetTeams({
                parameters: {
                    page: undefined,
                    pageSize: this.store.selectSnapshot(TeamsState.metadata).pageSize,
                    filter: data,
                    sortOrder: undefined,
                    sortColumn: undefined,
                },
            }),
        );
    }

    onSubmit() {
        const schedule: CreateScheduleDto = {
            ...this.matchForm.getRawValue(),
            date: this.matchForm.controls.date.value.toUTCString(),
            teams: [...this.selectedTeams],
        };
        this.store.dispatch(new AddMatch({ schedule }));
    }

    ngOnDestroy() {
        this.onDestroy.next(true);
        this.onDestroy.complete();
    }

    onUpsertTeam(): void {
        const team = <TeamDto>this.teamForm.getRawValue();
        if (!team.id) {
            this.store.dispatch(new AddTeam(team));
        } else {
            this.store.dispatch(new UpdateTeam({ teamId: <number>team.id, team }));
        }
    }

    changeModalVisibility(status: boolean) {
        this.isModalVisible = status;
    }

    onEdit(team: TeamDto): void {
        this.teamForm.controls.id.setValue(<number>team.id);
        this.teamForm.controls.name.setValue(<string>team.name);
        this.teamForm.controls.userIdentifier.setValue(<string>team.userIdentifier);
        this.changeModalVisibility(true);
    }

    onDelete(team: TeamDto) {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.store.dispatch(new DeleteTeam(<number>team.id));
                this.teamForm.reset();
            },
            reject: (type: any) => {
                switch (type) {
                    case ConfirmEventType.REJECT:
                        this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected' });
                        break;
                    case ConfirmEventType.CANCEL:
                        this.messageService.add({ severity: 'warn', summary: 'Cancelled', detail: 'You have cancelled' });
                        break;
                }
            },
        });
    }
}
