import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameplansCreateViewComponent } from './gameplans-create-view.component';
import { NgxsModule } from '@ngxs/store';
import { ReactiveFormsModule } from '@angular/forms';
import { provideAutoSpy } from 'jest-auto-spies';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MockModule } from 'ng-mocks';

describe('GameplansCreateViewComponent', () => {
    let component: GameplansCreateViewComponent;
    let fixture: ComponentFixture<GameplansCreateViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GameplansCreateViewComponent],
            imports: [NgxsModule.forRoot(), ReactiveFormsModule, CalendarModule, MockModule(ConfirmDialogModule)],
            providers: [provideAutoSpy(ConfirmationService), provideAutoSpy(MessageService)],
        }).compileComponents();

        fixture = TestBed.createComponent(GameplansCreateViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
