import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameplansDetailViewComponent } from './gameplans-detail-view.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { provideAutoSpy } from 'jest-auto-spies';
import { ConfirmationService, MessageService } from 'primeng/api';
import { MatchesService } from '@tournament-tracker/api';

describe('GameplansDetailViewComponent', () => {
    let component: GameplansDetailViewComponent;
    let fixture: ComponentFixture<GameplansDetailViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GameplansDetailViewComponent],
            imports: [HttpClientTestingModule, RouterTestingModule],
            providers: [provideAutoSpy(ConfirmationService), provideAutoSpy(MessageService), provideAutoSpy(MatchesService)],
        }).compileComponents();

        fixture = TestBed.createComponent(GameplansDetailViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
