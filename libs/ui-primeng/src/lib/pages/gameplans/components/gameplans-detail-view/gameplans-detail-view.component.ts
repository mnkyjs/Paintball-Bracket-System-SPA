import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockForViewDto, MatchesService } from '@tournament-tracker/api';
import { ConfirmationService, ConfirmEventType, MessageService } from 'primeng/api';
import { Observable, Subject, takeUntil } from 'rxjs';

@Component({
    selector: 'tt-ui-prime-gameplans-detail-view',
    templateUrl: './gameplans-detail-view.component.html',
    styleUrls: ['./gameplans-detail-view.component.scss'],
})
export class GameplansDetailViewComponent implements OnInit, OnDestroy {
    blockView$?: Observable<BlockForViewDto>;

    guid?: string;

    displayModal?: boolean;

    onDestry: Subject<void> = new Subject<void>();

    constructor(
        private matchesService: MatchesService,
        private route: ActivatedRoute,
        private router: Router,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
    ) {}

    ngOnInit() {
        this.guid = <string>this.route.snapshot.queryParamMap.get('guid');
        this.blockView$ = this.matchesService.getMatchesByGuid(this.guid);
    }

    onShowModalDialog() {
        this.displayModal = true;
    }

    onDelete() {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.matchesService
                    .deleteMatch(<string>this.guid)
                    .pipe(takeUntil(this.onDestry))
                    .subscribe(() => this.router.navigate(['/matches']));
            },
            reject: (type: any) => {
                switch (type) {
                    case ConfirmEventType.REJECT:
                        this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected' });
                        break;
                    case ConfirmEventType.CANCEL:
                        this.messageService.add({ severity: 'warn', summary: 'Cancelled', detail: 'You have cancelled' });
                        break;
                }
            },
        });
    }

    onPrint(): void {
        const printWin = window.open('');
        printWin?.document.write(document.getElementById('block-wrapper')?.innerHTML ?? '');
        printWin?.print();
    }

    ngOnDestroy(): void {
        this.onDestry.next();
        this.onDestry.complete();
    }
}
