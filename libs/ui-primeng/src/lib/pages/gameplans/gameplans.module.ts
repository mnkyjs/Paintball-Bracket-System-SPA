import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ApiModule, MatchesStateModule, TeamStateModule } from '@tournament-tracker/api';
import { IsOwnerOrAdminDirective } from '@tournament-tracker/utils';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { RippleModule } from 'primeng/ripple';
import { SkeletonModule } from 'primeng/skeleton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DialogComponent } from '../../components/dialog/dialog/dialog.component';
import { MainLayoutComponent } from '../../components/main-layout/main-layout.component';
import { PagingComponent } from '../../components/paging/paging.component';
import { GameplansCreateViewComponent } from './components/gameplans-create-view/gameplans-create-view.component';
import { GameplansDetailViewComponent } from './components/gameplans-detail-view/gameplans-detail-view.component';
import { GameplansListViewComponent } from './components/gameplans-list-view/gameplans-list-view.component';
import { GameplansResolver } from './services/gameplans.resolver';
import { TeamsResolver } from './services/teams.resolver';

@NgModule({
    declarations: [GameplansListViewComponent, GameplansDetailViewComponent, GameplansCreateViewComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: MainLayoutComponent,
                children: [
                    {
                        path: '',
                        component: GameplansListViewComponent,
                    },
                    {
                        path: 'create',
                        component: GameplansCreateViewComponent,
                        resolve: {
                            teams: TeamsResolver,
                        },
                    },
                    {
                        path: 'details',
                        component: GameplansDetailViewComponent,
                        resolve: {
                            gameplan: GameplansResolver,
                        },
                    },
                ],
            },
        ]),
        TeamStateModule,
        MatchesStateModule,
        CardModule,
        ButtonModule,
        RippleModule,
        SkeletonModule,
        TableModule,
        PagingComponent,
        InputTextModule,
        ReactiveFormsModule,
        CalendarModule,
        DialogComponent,
        IsOwnerOrAdminDirective,
        ConfirmDialogModule,
        ToastModule,
        DialogModule,
        ApiModule,
    ],
    providers: [ConfirmationService, MessageService],
})
export class GameplansModule {}
