import { TestBed } from '@angular/core/testing';

import { GameplansResolver } from './gameplans.resolver';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('GameplansResolver', () => {
    let resolver: GameplansResolver;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });
        resolver = TestBed.inject(GameplansResolver);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });
});
