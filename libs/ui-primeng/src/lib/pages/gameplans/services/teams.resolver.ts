import { Injectable } from '@angular/core';

import { Store } from '@ngxs/store';
import { DEFAULTPARAMETERS, GetTeams } from '@tournament-tracker/api';
import { map, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class TeamsResolver {
    constructor(private store: Store) {}

    resolve(): Observable<boolean> {
        return this.store
            .dispatch(
                new GetTeams({
                    parameters: {
                        ...DEFAULTPARAMETERS,
                        pageSize: 5,
                    },
                }),
            )
            .pipe(map(() => true));
    }
}
