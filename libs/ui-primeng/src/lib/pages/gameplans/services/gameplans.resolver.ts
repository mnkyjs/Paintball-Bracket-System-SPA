import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MatchesService } from '@tournament-tracker/api';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class GameplansResolver {
    constructor(private matchesService: MatchesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const guid = <string>route.queryParamMap.get('guid');
        return of(true);
    }
}
