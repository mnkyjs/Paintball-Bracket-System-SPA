import { TestBed } from '@angular/core/testing';

import { TeamsResolver } from './teams.resolver';
import { provideAutoSpy } from 'jest-auto-spies';
import { Store } from '@ngxs/store';

describe('TeamsResolver', () => {
    let resolver: TeamsResolver;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [provideAutoSpy(Store)],
        });
        resolver = TestBed.inject(TeamsResolver);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });
});
