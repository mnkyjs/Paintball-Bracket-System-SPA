import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccordionModule } from 'primeng/accordion';
import { FaqComponent } from './components/faq/faq.component';

import { FaqRoutingModule } from './faq-routing.module';

@NgModule({
    declarations: [FaqComponent],
    imports: [CommonModule, FaqRoutingModule, AccordionModule],
})
export class FaqModule {}
