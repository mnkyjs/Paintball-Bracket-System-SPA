import { AsyncPipe, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { FeatureState } from '@tournament-tracker/common';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { Observable } from 'rxjs';

@Component({
    selector: 'tt-ui-prime-loading-indicator',
    standalone: true,
    imports: [ProgressSpinnerModule, AsyncPipe, NgIf],
    template: `
        <div class="flex align-content-center justify-content-center">
            <p-progressSpinner *ngIf="loading$ | async"></p-progressSpinner>
        </div>
    `,
    styleUrls: ['./loading-indicator.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingIndicatorComponent {
    @Select(FeatureState.isLoading)
    loading$?: Observable<boolean>;
}
