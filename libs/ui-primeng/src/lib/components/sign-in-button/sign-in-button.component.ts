import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
    selector: 'tt-ui-prime-sign-in-button',
    standalone: true,
    styleUrls: ['./sign-in-button.component.scss'],
    template: `
        <a
            class="cursor-pointer flex surface-border mb-3 p-3 align-items-center border-1 surface-border border-round hover:surface-hover transition-colors transition-duration-150"
            (click)="onLogin()">
            <span>
                <i class="pi pi-power-off text-xl text-primary"></i>
            </span>
            <div class="ml-3">
                <span class="mb-2 font-semibold">Sign In</span>
                <p class="text-color-secondary m-0">Sign in to create new plans</p>
            </div>
        </a>
    `,
})
export class SignInButtonComponent {
    constructor(private oAuthService: OAuthService) {}

    onLogin() {
        this.oAuthService.initCodeFlow();
    }
}
