import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInButtonComponent } from './sign-in-button.component';
import { provideAutoSpy } from 'jest-auto-spies';
import { OAuthService } from 'angular-oauth2-oidc';

describe('SignInButtonComponent', () => {
    let component: SignInButtonComponent;
    let fixture: ComponentFixture<SignInButtonComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [SignInButtonComponent],
            providers: [provideAutoSpy(OAuthService)],
        }).compileComponents();

        fixture = TestBed.createComponent(SignInButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
