import { NgIf } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { ToolbarModule } from 'primeng/toolbar';

@Component({
    selector: 'tt-ui-prime-toolbar',
    standalone: true,
    imports: [ToolbarModule, ButtonModule, RippleModule, NgIf, RouterLink],
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
    @Input()
    showHamburgerButton: boolean | null = false;

    @Output()
    toggleSidebar: EventEmitter<void> = new EventEmitter<void>();

    @Output()
    toggleProfile: EventEmitter<void> = new EventEmitter<void>();
}
