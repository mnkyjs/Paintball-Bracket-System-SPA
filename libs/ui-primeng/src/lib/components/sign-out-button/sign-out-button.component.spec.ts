import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignOutButtonComponent } from './sign-out-button.component';
import { provideAutoSpy } from 'jest-auto-spies';
import { OAuthService } from 'angular-oauth2-oidc';
import { NgxsModule } from '@ngxs/store';

describe('SignOutButtonComponent', () => {
    let component: SignOutButtonComponent;
    let fixture: ComponentFixture<SignOutButtonComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [SignOutButtonComponent, NgxsModule.forRoot()],
            providers: [provideAutoSpy(OAuthService)],
        }).compileComponents();

        fixture = TestBed.createComponent(SignOutButtonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
