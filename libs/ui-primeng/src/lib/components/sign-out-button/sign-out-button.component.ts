import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { SetSignInState } from '@tournament-tracker/auth';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
    selector: 'tt-ui-prime-sign-out-button',
    standalone: true,
    styleUrls: ['./sign-out-button.component.scss'],
    template: ` <a
        class="cursor-pointer flex surface-border mb-3 p-3 align-items-center border-1 surface-border border-round hover:surface-hover transition-colors transition-duration-150"
        (click)="onLogout()">
        <span>
            <i class="pi pi-power-off text-xl text-primary"></i>
        </span>
        <div class="ml-3">
            <span class="mb-2 font-semibold">Sign Out</span>
        </div>
    </a>`,
})
export class SignOutButtonComponent {
    constructor(private oAuthService: OAuthService, private store: Store) {}

    onLogout() {
        this.oAuthService.logoutUrl = window.location.origin;
        this.oAuthService.logOut();
        this.store.dispatch(new SetSignInState({ isLoggedIn: false }));
    }
}
