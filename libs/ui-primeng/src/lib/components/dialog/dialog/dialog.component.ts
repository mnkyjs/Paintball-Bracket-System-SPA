import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';

@Component({
    selector: 'tt-ui-prime-dialog',
    standalone: true,
    imports: [DialogModule, ButtonModule],
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
    @Input()
    submitButtonLabel = '';

    @Input()
    cancelButtonLabel = '';

    @Input()
    isVisible = false;

    @Input()
    isDisabled = false;

    @Output()
    confirm: EventEmitter<void> = new EventEmitter<void>();

    @Output()
    cancel: EventEmitter<void> = new EventEmitter<void>();
}
