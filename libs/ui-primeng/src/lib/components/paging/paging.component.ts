import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Metadata, PageEvent } from '@tournament-tracker/api';
import { PaginatorModule } from 'primeng/paginator';

@Component({
    selector: 'tt-ui-prime-paging',
    standalone: true,
    imports: [CommonModule, PaginatorModule],
    template: `
        <ng-container *ngIf="metaData">
            <p-paginator
                [rowsPerPageOptions]="pageSizeOptions"
                [rows]="metaData.pageSize!"
                [totalRecords]="metaData.totalCount!"
                (onPageChange)="pageOptionChanged($event)">
            </p-paginator>
        </ng-container>
    `,
    styleUrls: ['./paging.component.scss'],
})
export class PagingComponent {
    @Input() metaData: Metadata | null = null;

    @Output() changePage: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

    pageSizeOptions: number[] = [5, 10, 25];

    pageOptionChanged($event: PageEvent): void {
        $event.page = $event.page + 1;
        this.changePage.emit($event);
    }
}
