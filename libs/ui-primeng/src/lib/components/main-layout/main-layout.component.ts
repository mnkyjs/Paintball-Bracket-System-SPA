import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { FeatureToggle } from '@tournament-tracker/api';
import { AuthState, SetSignInState } from '@tournament-tracker/auth';
import { FeatureState } from '@tournament-tracker/common';
import { OAuthEvent, OAuthService } from 'angular-oauth2-oidc';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { SidebarModule } from 'primeng/sidebar';
import { Observable } from 'rxjs';
import { LoadingIndicatorComponent } from '../loading-indicator/loading-indicator.component';
import { SignInButtonComponent } from '../sign-in-button/sign-in-button.component';
import { SignOutButtonComponent } from '../sign-out-button/sign-out-button.component';
import { ToolbarComponent } from '../toolbar/toolbar.component';

@Component({
    selector: 'tt-ui-prime-main-layout',
    standalone: true,
    imports: [
        CommonModule,
        ButtonModule,
        RippleModule,
        ToolbarComponent,
        RouterOutlet,
        SidebarModule,
        SignOutButtonComponent,
        SignInButtonComponent,
        RouterLink,
        LoadingIndicatorComponent,
    ],
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
    @Select(AuthState.isLoggedIn)
    isLoggedIn$?: Observable<boolean>;

    isSidebarFeatureEnabled$: Observable<boolean> = this.store.select(FeatureState.isFeatureEnabled(FeatureToggle.ShowSidebarNavigation));

    showSlideMenu = false;

    showProfileMenu = false;

    constructor(private oAuthService: OAuthService, private store: Store) {}

    ngOnInit(): void {
        this.oAuthService.events.subscribe((event: OAuthEvent) => {
            if (event.type === 'token_received') {
                this.store.dispatch(new SetSignInState({ isLoggedIn: true }));
            }
        });

        document.getElementById('app-not-ready')?.remove();
    }

    onToggleSlideMenu(): void {
        this.showSlideMenu = !this.showSlideMenu;
        this.showProfileMenu = false;
    }

    onToggleProfileMenu(): void {
        this.showProfileMenu = !this.showProfileMenu;
    }
}
