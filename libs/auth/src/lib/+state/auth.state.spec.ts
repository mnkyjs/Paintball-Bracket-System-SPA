import { Store, NgxsModule } from '@ngxs/store';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideAutoSpy } from 'jest-auto-spies';
import { OAuthService } from 'angular-oauth2-oidc';
import { SetSignInState } from './auth.actions';
import { AuthState } from './auth.state';

describe('Auth State', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([AuthState]), HttpClientTestingModule],
            providers: [provideAutoSpy(OAuthService)],
            teardown: { destroyAfterEach: false },
        });
        store = TestBed.inject(Store);
    });

    it('should set logged in state', () => {
        store.dispatch(new SetSignInState({ isLoggedIn: true }));

        expect(store.selectSnapshot(AuthState.isLoggedIn)).toBe(true);
    });
});
