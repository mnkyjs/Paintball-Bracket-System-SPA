export class SetSignInState {
    public static readonly type = '[Auth] Set Sign In State';

    constructor(public payload: { isLoggedIn: boolean }) {}
}
