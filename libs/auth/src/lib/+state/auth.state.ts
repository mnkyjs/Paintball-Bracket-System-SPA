import { Injectable } from '@angular/core';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { OAuthService } from 'angular-oauth2-oidc';
import { SetSignInState } from './auth.actions';

export interface AuthStateModel {
    isLoggedIn: boolean;
}

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        isLoggedIn: false,
    },
})
@Injectable()
export class AuthState implements NgxsOnInit {
    @Selector([AuthState])
    static isLoggedIn({ isLoggedIn }: AuthStateModel): boolean {
        return isLoggedIn;
    }

    constructor(private oAuthService: OAuthService) {}

    ngxsOnInit({ patchState }: StateContext<AuthStateModel>) {
        patchState({
            isLoggedIn: this.oAuthService.hasValidAccessToken(),
        });
    }

    @Action(SetSignInState)
    setSignInState({ patchState }: StateContext<AuthStateModel>, { payload }: SetSignInState) {
        const { isLoggedIn } = payload;
        return patchState({
            isLoggedIn,
        });
    }
}
