import { HTTP_INTERCEPTORS, HttpClient, HttpInterceptor } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { Store } from '@ngxs/store';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SetLoadingIndicator } from '../+state';

import { ApiStatusInterceptor } from './api-status.interceptor';

describe('ApiStatusInterceptor', () => {
    let interceptor: ApiStatusInterceptor;
    const mockStore: any = { dispatch: jest.fn() };
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeAll(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                { provide: HTTP_INTERCEPTORS, useClass: ApiStatusInterceptor, multi: true },
                { provide: Store, useValue: mockStore },
            ],
            teardown: { destroyAfterEach: false },
        });

        interceptor = (<HttpInterceptor[]>TestBed.inject(HTTP_INTERCEPTORS)).find(
            (service) => service instanceof ApiStatusInterceptor,
        ) as ApiStatusInterceptor;
        httpMock = TestBed.inject(HttpTestingController);
        httpClient = TestBed.inject(HttpClient);
    });

    it('should be created', () => {
        expect(interceptor).toBeTruthy();
    });

    it('should dispatch SetLoading for a single request', waitForAsync(() => {
        httpClient.get<any>('/api/is/this/a/test').subscribe();
        expect(mockStore.dispatch).toHaveBeenCalledWith(new SetLoadingIndicator({ isLoading: true }));

        httpMock.expectOne('/api/is/this/a/test').flush({});
        expect(mockStore.dispatch).toHaveBeenCalledWith(new SetLoadingIndicator({ isLoading: false }));
        httpMock.verify();
    }));

    it('should dispatch SetLoading for a single request when it fails', waitForAsync(() => {
        httpClient
            .get<any>('/api/is/this/a/test')
            .pipe(catchError(() => of(null)))
            .subscribe();
        expect(mockStore.dispatch).toHaveBeenCalledWith(new SetLoadingIndicator({ isLoading: true }));

        httpMock.expectOne('/api/is/this/a/test').error({} as ErrorEvent);
        expect(mockStore.dispatch).toHaveBeenCalledWith(new SetLoadingIndicator({ isLoading: false }));
        httpMock.verify();
    }));

    it('should dispatch SetLoading for multiple requests', waitForAsync(() => {
        mockStore.dispatch.mockReset();
        httpClient
            .get<any>('/api/is/this/a/test')
            .pipe(catchError(() => of(null)))
            .subscribe();
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);
        expect(mockStore.dispatch).toHaveBeenCalledWith(new SetLoadingIndicator({ isLoading: true }));

        httpClient
            .get<any>('/api/is/this/a/test')
            .pipe(catchError(() => of(null)))
            .subscribe();
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);

        httpClient
            .get<any>('/api/is/this/a/test')
            .pipe(catchError(() => of(null)))
            .subscribe();
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);

        const requests = httpMock.match('/api/is/this/a/test');
        requests[0].flush({});
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);

        requests[1].error({} as ErrorEvent);
        expect(mockStore.dispatch).toHaveBeenCalledTimes(1);

        requests[2].flush({});
        expect(mockStore.dispatch).toHaveBeenCalledTimes(2);
        expect(mockStore.dispatch).toHaveBeenCalledWith(new SetLoadingIndicator({ isLoading: false }));
        httpMock.verify();
    }));
});
