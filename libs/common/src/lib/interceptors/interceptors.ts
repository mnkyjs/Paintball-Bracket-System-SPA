import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiStatusInterceptor } from './api-status.interceptor';

export const httpInterceptors = [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: ApiStatusInterceptor,
        multi: true,
    },
    // ... weitere Interceptoren
];
