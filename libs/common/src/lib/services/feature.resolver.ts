import { Injectable } from '@angular/core';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetFeatures } from '../+state/index';

@Injectable({
    providedIn: 'root',
})
export class FeatureResolver {
    constructor(private store: Store) {}

    resolve(): Observable<boolean> {
        return this.store.dispatch(new GetFeatures());
    }
}
