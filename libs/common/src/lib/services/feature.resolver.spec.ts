import { TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';

import { FeatureResolver } from './feature.resolver';

describe('FeatureResolver', () => {
    let resolver: FeatureResolver;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([])],
        });
        resolver = TestBed.inject(FeatureResolver);
    });

    it('should be created', () => {
        expect(resolver).toBeTruthy();
    });
});
