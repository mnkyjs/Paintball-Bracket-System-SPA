import { Injectable } from '@angular/core';
import { Action, createSelector, Selector, State, StateContext } from '@ngxs/store';
import { Feature, FeatureService, FeatureToggle } from '@tournament-tracker/api';
import { EMPTY } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { GetFeatures, SetLoadingIndicator, SetToggleState } from './feature.actions';

export interface FeatureStateModel {
    isLoading: boolean;
    toggleSwitch: boolean;
    features: Feature[];
}

const defaults: FeatureStateModel = {
    isLoading: false,
    toggleSwitch: true,
    features: [],
};

@State<FeatureStateModel>({
    name: 'feature',
    defaults,
})
@Injectable()
export class FeatureState {
    @Selector([FeatureState])
    static isLoading({ isLoading }: FeatureStateModel): boolean {
        return isLoading;
    }

    @Selector([FeatureState])
    static toggleSwitch({ toggleSwitch }: FeatureStateModel): boolean {
        return JSON.parse(sessionStorage.getItem('toggleSwitch') ?? 'null') ?? toggleSwitch;
    }

    static isFeatureEnabled(feature: FeatureToggle) {
        return createSelector([FeatureState], ({ features }: FeatureStateModel): boolean => {
            return <boolean>features.find((feat) => feat.featureToggle === feature)?.isEnabled;
        });
    }

    constructor(private featureService: FeatureService) {}

    @Action(GetFeatures)
    getFeatures({ patchState }: StateContext<FeatureStateModel>) {
        return this.featureService.getFeatures().pipe(
            tap((features: Feature[]) =>
                patchState({
                    features,
                }),
            ),
            catchError((err) => {
                return EMPTY;
            }),
        );
    }

    @Action(SetLoadingIndicator)
    loadingIndicator({ patchState }: StateContext<FeatureStateModel>, { payload }: SetLoadingIndicator) {
        const { isLoading } = payload;
        patchState({ isLoading });
    }

    @Action(SetToggleState)
    setToggleState({ patchState }: StateContext<FeatureStateModel>, { payload }: SetToggleState) {
        const { toggleSwitch } = payload;
        sessionStorage.setItem('toggleSwitch', toggleSwitch + '');
        patchState({ toggleSwitch });
    }
}
