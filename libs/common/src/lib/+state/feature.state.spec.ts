import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { SetLoadingIndicator, SetToggleState } from './feature.actions';
import { FeatureState } from './feature.state';

describe('Feature actions', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([FeatureState]), HttpClientTestingModule],
            providers: [],
            teardown: { destroyAfterEach: false },
        });
        store = TestBed.inject(Store);
    });

    it('should toggle switch to true', () => {
        store.dispatch(new SetToggleState({ toggleSwitch: true }));
        store
            .select((state) => state.feature.toggleSwitch)
            .subscribe((toggleSwitch: boolean) => {
                expect(toggleSwitch).toBeTruthy();
            });
    });

    it('should set loading state to true', () => {
        store.dispatch(new SetLoadingIndicator({ isLoading: true }));
        expect(store.selectSnapshot(FeatureState.isLoading)).toBeTruthy();
    });
});
