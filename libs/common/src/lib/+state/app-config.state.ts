import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SwitchLang, SwitchTheme } from './app-config.actions';

export interface AppConfigStateModel {
    isDarkThemeSelected: boolean;
    language: string;
}

const defaults = {
    isDarkThemeSelected: false,
    language: 'en',
};

@State<AppConfigStateModel>({
    name: 'appConfig',
    defaults,
})
@Injectable()
export class AppConfigState {
    @Selector([AppConfigState])
    static isDarkThemeSelected({ isDarkThemeSelected }: AppConfigStateModel): boolean {
        return isDarkThemeSelected;
    }

    @Selector([AppConfigState])
    static language({ language }: AppConfigStateModel): string {
        return language;
    }

    @Action(SwitchTheme)
    switchTheme({ patchState }: StateContext<AppConfigStateModel>, { payload: { isDarkThemeSelected } }: SwitchTheme) {
        patchState({ isDarkThemeSelected });
    }

    @Action(SwitchLang)
    switchLang({ patchState }: StateContext<AppConfigStateModel>, { payload: { language } }: SwitchLang) {
        patchState({ language });
        // this.translocoService.setActiveLang(language);
        localStorage.setItem('lang', language);
    }
}
