import { CreateScheduleDto } from '../../openapi/index';

export class AddMatch {
    public static readonly type = '[Matches] Add match';

    constructor(public payload: { schedule: CreateScheduleDto }) {}
}
