import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { MessageService } from 'primeng/api';
import { EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { BlockDto, MatchesService, TeamDto } from '../../openapi/index';
import { AddMatch } from './matches.actions';

export interface MatchesStateModel {
    blocks: BlockDto[];
}

@State<MatchesStateModel>({
    name: 'matches',
    defaults: {
        blocks: [],
    },
})
@Injectable()
export class MatchesState {
    @Selector()
    static blocks({ blocks }: MatchesStateModel): BlockDto[] {
        return blocks;
    }

    constructor(private matchesService: MatchesService, private messageService: MessageService, private router: Router) {}

    @Action(AddMatch)
    public add(ctx: StateContext<MatchesStateModel>, { payload }: AddMatch): Observable<TeamDto[][]> {
        const { schedule } = payload;
        return this.matchesService.createSchedule(schedule).pipe(
            tap(() => {
                this.showSuccess();
                this.router.navigate(['/matches']);
            }),
            catchError((err) => {
                this.showError();
                return EMPTY;
            }),
        );
    }

    private showSuccess() {
        this.messageService.add({ severity: 'success', summary: 'Saved', detail: 'Everything saved!' });
    }

    private showError(): void {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something went wrong, please try again' });
    }
}
