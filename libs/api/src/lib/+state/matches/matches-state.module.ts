import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { MatchesState } from './matches.state';

@NgModule({
    imports: [NgxsModule.forFeature([MatchesState])],
})
export class MatchesStateModule {}
