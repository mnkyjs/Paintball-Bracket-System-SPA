import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { MessageService } from 'primeng/api';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Metadata, TeamDto, TeamDtoPagedResult, TeamsService } from '../../openapi/index';
import { AddTeam, DeleteTeam, GetTeams, UpdateTeam } from './teams.actions';

export interface TeamsStateModel {
    teams: TeamDto[];
    metadata: Metadata;
    selectedTeamId: number | undefined;
}

@State<TeamsStateModel>({
    name: 'teams',
    defaults: {
        teams: [],
        metadata: {},
        selectedTeamId: undefined,
    },
})
@Injectable()
export class TeamsState {
    @Selector()
    public static getState(state: TeamsStateModel) {
        return state;
    }

    @Selector()
    static teams({ teams }: TeamsStateModel): TeamDto[] {
        return teams;
    }

    @Selector()
    static metadata({ metadata }: TeamsStateModel): Metadata {
        return metadata;
    }

    constructor(private teamsService: TeamsService, private messageService: MessageService) {}

    @Action(AddTeam)
    public add({ patchState, getState }: StateContext<TeamsStateModel>, { payload }: AddTeam): Observable<TeamDto> {
        return this.teamsService.postTeam(payload).pipe(
            tap((team: TeamDto) => {
                patchState({
                    teams: [...(getState().teams ?? []), team],
                });
                this.showSuccess();
            }),
            catchError((err) => {
                this.showError();
                return throwError(err);
            }),
        );
    }

    @Action(GetTeams)
    public getTeams({ patchState }: StateContext<TeamsStateModel>, { payload }: GetTeams): Observable<TeamDtoPagedResult> {
        const { page, pageSize, sortColumn, sortOrder, filter } = payload.parameters;
        return this.teamsService.apiTeamsGet(page, pageSize, filter, sortColumn, sortOrder).pipe(
            tap((result: TeamDtoPagedResult) =>
                patchState({
                    teams: result.items ?? [],
                    metadata: result.metadata,
                }),
            ),
            catchError((err) => {
                this.showError();
                return throwError(err);
            }),
        );
    }

    @Action(DeleteTeam)
    public deleteTeam({ patchState, getState }: StateContext<TeamsStateModel>, { payload }: DeleteTeam): Observable<void> {
        return this.teamsService.deleteTeam(payload).pipe(
            tap(() => {
                patchState({ teams: [...getState().teams.filter((team) => team.id !== payload)] });
                this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' });
            }),
            catchError((err) => {
                this.showError();
                return throwError(err);
            }),
        );
    }

    @Action(UpdateTeam)
    public updateTeam({ getState, patchState }: StateContext<TeamsStateModel>, { payload }: UpdateTeam): Observable<TeamDto> {
        return this.teamsService.updateTeam(payload.teamId, payload.team as TeamDto).pipe(
            tap((response) => {
                patchState({ teams: [...getState().teams.map((item) => (item.id === response.id ? response : item))] });
                this.showSuccess();
            }),
            catchError((err) => {
                this.showError();
                return throwError(err);
            }),
        );
    }

    private showSuccess() {
        this.messageService.add({ severity: 'success', summary: 'Saved', detail: 'Everything saved!' });
    }

    private showError(): void {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Something went wrong, please try again' });
    }
}
