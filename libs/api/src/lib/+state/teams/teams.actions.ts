import { ApiCallParameters } from '../../models/index';
import { TeamDto } from '../../openapi/index';

export class AddTeam {
    public static readonly type = '[Teams] Add item';

    constructor(public payload: TeamDto) {}
}

export class GetTeams {
    public static readonly type = '[Teams] Get all';

    constructor(public payload: { parameters: ApiCallParameters }) {}
}

export class DeleteTeam {
    public static readonly type = '[Teams] Delete';

    constructor(public payload: number) {}
}

export class UpdateTeam {
    public static readonly type = '[Teams] Update';

    constructor(public payload: { teamId: number; team: Partial<TeamDto> }) {}
}
