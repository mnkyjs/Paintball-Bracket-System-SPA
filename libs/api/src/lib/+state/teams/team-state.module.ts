import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { TeamsState } from './teams.state';

@NgModule({
    imports: [NgxsModule.forFeature([TeamsState])],
})
export class TeamStateModule {}
