import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule, Store } from '@ngxs/store';
import { provideAutoSpy } from 'jest-auto-spies';
import { MessageService } from 'primeng/api';
import { of, throwError } from 'rxjs';
import { DEFAULTPARAMETERS } from '../../models/index';
import { ApiModule, TeamDto, TeamDtoPagedResult, TeamsService } from '../../openapi/index';
import { Team } from '../../openapi/model/team';
import { AddTeam, DeleteTeam, GetTeams, UpdateTeam } from './teams.actions';
import { TeamsState, TeamsStateModel } from './teams.state';

const TEAM_EXAMPLE = { id: 1, name: 'Test 123', user: 'User_1' } as Team;

const TEAMS_EXAMPLE = [
    { ...TEAM_EXAMPLE },
    {
        ...TEAM_EXAMPLE,
        id: 23,
    },
    { ...TEAM_EXAMPLE, name: 'Test 1234' },
];

const PAGED_RESULT_EXAMPLE: TeamDtoPagedResult = {
    items: [{ id: 1, name: 'Test 123', userIdentifier: 'User_1' }],
};

describe('Teams store', () => {
    let store: Store;
    let service: TeamsService;
    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([TeamsState]), ApiModule, HttpClientTestingModule, BrowserAnimationsModule],
            providers: [provideAutoSpy(MessageService)],
            teardown: { destroyAfterEach: false },
        }).compileComponents();
        store = TestBed.inject(Store);
        service = TestBed.inject(TeamsService);
    }));

    it('should create an action and add an item', () => {
        jest.spyOn(service, 'postTeam').mockReturnValue(of());
        const expected: TeamsStateModel = {
            teams: [],
            metadata: {},
            selectedTeamId: undefined,
        };
        store.dispatch(new AddTeam({} as TeamDto));
        const actual = store.selectSnapshot(TeamsState.getState);
        expect(actual).toEqual(expected);
    });

    it('should select teams from state', waitForAsync(() => {
        service.postTeam = jest.fn().mockReturnValue(of(TEAM_EXAMPLE));
        store.dispatch(new AddTeam(TEAM_EXAMPLE as TeamDto));
        expect(store.selectSnapshot(TeamsState.teams)).toEqual([TEAM_EXAMPLE]);
    }));

    it('should remove team from state', waitForAsync(() => {
        service.postTeam = jest.fn().mockReturnValue(of(TEAM_EXAMPLE));
        service.deleteTeam = jest.fn().mockReturnValue(of(true));

        store.dispatch(new AddTeam(TEAM_EXAMPLE as TeamDto));
        store.dispatch(new DeleteTeam(1));

        expect(service.deleteTeam).toHaveBeenCalledTimes(1);
        expect(store.selectSnapshot(TeamsState.teams)).toEqual([]);
    }));

    it('should return current state', () => {
        const expected: TeamsStateModel = {
            teams: [],
            metadata: {},
            selectedTeamId: undefined,
        };

        expect(store.selectSnapshot(TeamsState.getState)).toEqual(expected);
        expect(store.selectSnapshot(TeamsState.teams)).toEqual([]);
    });

    it('should update given team', waitForAsync(() => {
        const UPDATE_TEAM = { ...TEAM_EXAMPLE, name: 'UPDATED' };
        service.updateTeam = jest.fn().mockReturnValue(of(UPDATE_TEAM));
        service.postTeam = jest.fn().mockReturnValue(of(TEAM_EXAMPLE));

        store.dispatch(new AddTeam(TEAM_EXAMPLE as TeamDto));
        store.dispatch(new UpdateTeam({ teamId: 1, team: UPDATE_TEAM }));

        expect(service.updateTeam).toHaveBeenCalledTimes(1);
        expect(store.selectSnapshot(TeamsState.teams)).toEqual([UPDATE_TEAM]);
    }));

    // it('should get all teams and set loaded to true', () => {
    //     const spy = jest.spyOn(service, 'apiTeamsGet').mockReturnValue(PAGED_RESULT_EXAMPLE);
    //
    //     store.dispatch(new GetTeams({ parameters: DEFAULTPARAMETERS }));
    //     expect(spy).toHaveBeenCalledTimes(1);
    //     expect(store.selectSnapshot(TeamsState.teams)).toEqual(TEAMS_EXAMPLE);
    // });

    it('should validate error block', () => {
        service.postTeam = jest.fn().mockReturnValue(throwError({ status: 404 }));
        service.apiTeamsGet = jest.fn().mockReturnValue(throwError({ status: 404 }));
        service.deleteTeam = jest.fn().mockReturnValue(throwError({ status: 404 }));
        service.updateTeam = jest.fn().mockReturnValue(throwError({ status: 404 }));

        store.dispatch(new GetTeams({ parameters: DEFAULTPARAMETERS }));
        store.dispatch(new DeleteTeam(9));
        store.dispatch(new UpdateTeam({ teamId: 3, team: TEAM_EXAMPLE as TeamDto }));
        store.dispatch(new AddTeam(TEAM_EXAMPLE as TeamDto));

        expect(service.postTeam).toHaveBeenCalledTimes(1);
        expect(service.apiTeamsGet).toHaveBeenCalledTimes(1);
        expect(service.deleteTeam).toHaveBeenCalledTimes(1);
        expect(service.updateTeam).toHaveBeenCalledTimes(1);
    });
});
