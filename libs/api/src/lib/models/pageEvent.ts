export interface PageEvent {
    first: number;
    page: number;
    pageCount: number;
    pageIndex: number;
    rows: number;
}
