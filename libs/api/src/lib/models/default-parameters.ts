export class ApiCallParameters {
    page: number | undefined;

    pageSize: number | undefined;

    filter: string | undefined;

    sortColumn: string | undefined;

    sortOrder: string | undefined;
}

export const DEFAULTPARAMETERS = {
    page: undefined,
    pageSize: undefined,
    filter: undefined,
    sortOrder: undefined,
    sortColumn: undefined,
} as ApiCallParameters;
