/**
 * BracketSystem API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Paintballfield } from './paintballfield';
import { Team } from './team';

export interface Match {
    id?: number;
    matchName?: string | null;
    date?: string | null;
    guid?: string | null;
    userIdentifier?: string | null;
    teamAId?: number;
    teamA?: Team;
    teamBId?: number;
    teamB?: Team;
    paintballfieldId?: number;
    paintballfield?: Paintballfield;
}
