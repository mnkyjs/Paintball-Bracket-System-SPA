export * from './feature.service';
import { FeatureService } from './feature.service';
import { MatchesService } from './matches.service';
import { TeamsService } from './teams.service';

export * from './matches.service';
export * from './teams.service';
export const APIS = [FeatureService, MatchesService, TeamsService];
