# nginx state for serving content
FROM nginx:1.20.2-alpine AS build

# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# Copy static assets from builder stage
ADD ./dist/apps/tournament-tracker .
ADD ./nginx.conf .
COPY /nginx.conf  /etc/nginx/conf.d/default.conf
EXPOSE 80
# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
