<h1 align="center">Welcome to Clashplaner 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
  <a href="https://twitter.com/mnkyjs" target="_blank">
    <img alt="Twitter: mnkyjs" src="https://img.shields.io/twitter/follow/mnkyjs.svg?style=social" />
  </a>
</p>

> An application to create and manage clashes.

### 🏠 [Homepage](https://clashplaner.uk.to/)

### ✨ [Demo](https://clashplaner.uk.to/)

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```

## Run tests

```sh
npm run test
```

## Author

👤 **Dennis Hundertmark**

-   Website: https://mnky-js.com/
-   Twitter: [@mnkyjs](https://twitter.com/mnkyjs)
-   Github: [@mnkyjs](https://github.com/mnkyjs)
-   LinkedIn: [@DennisHundertmark](https://linkedin.com/in/DennisHundertmark)

## Show your support

Give a ⭐️ if this project helped you!

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
